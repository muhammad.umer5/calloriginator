var Koa = require('koa');
var Router = require('koa-router');
const cors = require('@koa/cors');
const call = require('./call')
var app = new Koa();
var router = new Router();

app.use(cors());
router.post('/call-orginator-api/:number/:channelFrom', async (ctx, next) => {
    // ctx.router available;
    try {
        const number = ctx.params.number;
        const myObj = ctx.response.body;
        const channelFrom = ctx.params.channelFrom
        //const extension = ctx.params.d;
        let result = await call({ ctx, number, channelFrom, myObj });
        ctx.status = 200;
        ctx.response.body = { message: "call orignated" }
    } catch (error) {
        console.log(error);
        ctx.status = 200;
        ctx.response.body = { message: "call orignated failed.", error: error.message }
    }

});

app
    .use(router.routes())
    .use(router.allowedMethods());


app.listen(4000);
