const util = require('util');
const exec = util.promisify(require("child_process").exec);
const axios = require(`axios`);
const call = async (payload) => {
    const { ctx, number, type, channelFrom } = payload;
    const command = `asterisk -rx "channel originate local/${number}_${channelFrom}@local_dial extension ${number}@defaultcontext"`;
    console.log(command);
    try {
        let { stdout } = await exec(`asterisk -rx "core show channels count"`);
        console.log(`concurent channels : ${parseInt(stdout)}`);
        if (parseInt(stdout) < 1000) {
            const response = await exec(command);
            // axios.get(`http://localhost:3009/getChannelInfo`, payload.myObj);
            return "success";
        } else {
            return "max. channel capacity reached"
        }

        //ctx.status = 200;
        //ctx.body = { message: "success", response }
    } catch (error) {
        console.log(error);

        //ctx.status = 400;
        //ctx.body = { message: "call orignated failed.", error: error.message }
    }
}

module.exports = call;
